FROM jekyll/jekyll:3.6

WORKDIR /srv/jekyll

COPY . .
#RUN jekyll new .

RUN bundle install

RUN jekyll build

CMD ["jekyll", "serve"]

ENTRYPOINT ["/usr/jekyll/bin/entrypoint"]






